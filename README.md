# ARQ News

## Installation

Clonez le repo, déplacez vous vers le dossier **arq-news-pwa** et installez les dépendences.

```bash
git clone https://gitlab.com/Reese73/arq-news.git
cd arq-news-pwa
npm install
```

## ARQ News - Back


### Importer le projet MAVEN de l'API avec Eclipse : 
1. importer le dossier :
    * api_rest
2. "Run as Maven generated sources"
3. "Update Maven project"
3. Lancer la classe "Application"

## ARQ News - Front

Ce projet est le site web du projet Multithreading de notre équipe.

Ce site web utilise un serveur **Node.js**.

### Utilisation

Pour démarrer le serveur, utilisez la commande suivante dans le dossier **arq-news-pwa**

```bash
npm run serve
```
Ouvrez [http://localhost:8081](http://localhost:8081) sur votre navigateur.

### Configuration

Le port est configurable dans le fichier **vue.config.js**

```js
module.exports = {
    devServer: {
        port: 8081
    }
}
```
