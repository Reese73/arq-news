package com.tlv.arqnews.rest_database;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.tlv.arqnews.rest_dao.NewsRepository;
import com.tlv.arqnews.rest_domain.News;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class LoadDatabase {
	
	@Bean
	CommandLineRunner initDatabase(NewsRepository newsRepository) {
		return args -> {
			log.info("Preloading" + newsRepository.save(new News("Karim Benzema de retour en equipe de France","Raymond DESCHAMPS","L'entraîneur Didier DESCHAMPS a confirme le retour de Karim BENZEMA en équipe de France pour l'Euro 2020. L'annonce du retour de Karim Benzema en bleu a déclenché une vague d'enthousiasme chez les Français. Mais aussi au sein de la sélection tricolore.")));
			log.info("Preloading" + newsRepository.save(new News("Droits TV : la LFP pense à lancer sa propre chaîne pour diffuser la Ligue 1","Raymond DESCHAMPS","Le feuilleton de la vente des droits de la Ligue 1 n'en finit plus. Pour l'heure, Canal + retransmet toutes les rencontres jusqu'à la fin de la saison après un accord passé avec la LFP, qui avait perdu Mediapro après seulement quelques mois")));
		};
	}
}
