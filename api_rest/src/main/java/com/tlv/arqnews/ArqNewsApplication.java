package com.tlv.arqnews;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArqNewsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArqNewsApplication.class, args);
	}

}
