package com.tlv.arqnews.rest_exceptions;

public class NewsNotFoundException extends RuntimeException {
	
	NewsNotFoundException(Long id){
		super("Could not find news" + id);
	}

}
