package com.tlv.arqnews.rest_service_impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tlv.arqnews.rest_dao.NewsRepository;
import com.tlv.arqnews.rest_domain.News;
import com.tlv.arqnews.rest_service.NewsService;

@Service
@Transactional
public class NewsServiceImpl implements NewsService {
	
	@Autowired
	private NewsRepository newsRepository;
	
	@Override
	@Transactional(readOnly = true)
	public List<News> findAllNews(){
		
		return this.newsRepository.findAll();
	}
	
	@Override
	//@Transactional(readOnly = true)
	public News addNews(News news) {
		
		return this.newsRepository.save(news);
	}
}
