package com.tlv.arqnews.rest_domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="NEWS")
public class News {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String title;
	
	private String author;
	
	//DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
	private LocalDateTime pDate = LocalDateTime.now(); 
	
	private String text;
	
	public News() {
		
	}
	
	public News(String title, String author, String text) {
		
		this.title = title;
		this.author = author;
		this.pDate = LocalDateTime.now();
		this.text = text;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public LocalDateTime getpDate() {
		return pDate;
	}
	
	public void setpDate(LocalDateTime pDate) {
		this.pDate = pDate;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
}
