package com.tlv.arqnews.rest_service;

import java.util.List;

import com.tlv.arqnews.rest_domain.News;

public interface NewsService {
	
	public List<News> findAllNews();
	
	public News addNews(News news);
}
