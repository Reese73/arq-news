package com.tlv.arqnews.rest_dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tlv.arqnews.rest_domain.News;

public interface NewsRepository extends JpaRepository<News, Long>{

}
