package com.tlv.arqnews.rest_controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tlv.arqnews.rest_domain.News;
import com.tlv.arqnews.rest_service.NewsService;

@RestController
public class NewsController {
	
	@Autowired
	private NewsService newsService;
	
	@CrossOrigin(origins = "http://localhost:8082")
	@GetMapping("/news")
	public List<News> findAllNews(){
		
		return this.newsService.findAllNews();
	}
	
	@CrossOrigin(origins = "http://localhost:8082")
	@PostMapping("/news")
	public News addNews(@RequestBody News news) {
		
		return this.newsService.addNews(news);
	}
}
